package com.example.szl7033.camera2preview;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Semaphore;

public class MainActivity extends AppCompatActivity {

    /**
     * 打印测试信息标志
     */
    private static final String TAG = "shi_main";

    /**
     * 创建预览控件实例
     */
    private TextureView mTextureView = null;

    /**
     * 预览控件的尺寸(用作求标准比例)
     */
    private int viewWidth;
    private int viewHeight;

    /**
     * 照片尺寸和预览尺寸
     */
    private Size previewSize;
    private Size pictureSize;

    /**
     * 捕获请求构建器（用于预览）
     */
    private CaptureRequest.Builder mPreviewRequestBuilder;

    /**
     * 处理线程同步问题
     */
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);

    /**
     * 图像捕获会话
     */
    private CameraCaptureSession mCaptureSession;

    /**
     * 相机设备
     */
    private CameraDevice mCameraDevice;

    /**
     * 所选相机支持的参数
     */
    private StreamConfigurationMap map;

    /**
     * 处理静态图像捕获
     */
    private ImageReader pictureImageReader;

    /**
     * 处理预览图像捕获
     */
    private ImageReader previewImageReader;

    /**
     * 预览控件表面/预览图像表面/拍照图像表面
     */
    Surface previewTextureSurface;
    Surface previewImageSurface;
    Surface pictureImageSurface;

    /**
     * 文件存储路径
     */
    private File mFile;

    /**
     * Android6.0 以上需要运行时权限授权
     */
    private void checkPermission(){
        String[] permissions = new String[]{
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        };
        List<String> mPermissionList = new ArrayList<>();

        mPermissionList.clear();
        for (String permission1 : permissions) {
            if (ContextCompat.checkSelfPermission(MainActivity.this, permission1) != PackageManager.PERMISSION_GRANTED) {
                mPermissionList.add(permission1);
            }
        }
        if (mPermissionList.isEmpty()) {//未授予的权限为空，表示都授予了
            Toast.makeText(MainActivity.this,"已经授权",Toast.LENGTH_LONG).show();
        } else {//请求权限方法
            String[] permission = mPermissionList.toArray(new String[0]);//将List转为数组
            ActivityCompat.requestPermissions(MainActivity.this, permission, 2);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //加载界面
        setContentView(R.layout.activity_main);
        //获取权限
        checkPermission();
        //控件操作(在控件的监听回调的方法中执行拍照和预览功能)
        initViews();
    }

    /**
     * 初始化控件，并设置监听
     */
    private void initViews(){
        //设置预览和预览监听
        mTextureView = findViewById(R.id.camera_texture_view);
        mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        //设置按钮和拍照监听
        Button mButton = findViewById(R.id.button_capture);
        mButton.setOnClickListener(mButtonClickListener);
    }

    /**
     * 初始化图像载体，并设置监听，即图像数据操作
     */
    private void initImageReaders(){
        //获取合理的预览/照片尺寸（以显示控件的宽高比为标准比例，因为是竖屏显示，所以标准比例为高宽比）
        float ratio = (float) viewHeight / viewWidth;

        setLog("view ratio:" + ratio);

        pictureSize = null;
        previewSize = null;
        float r = 0;
        Size[] imageSizes = map.getOutputSizes(ImageFormat.JPEG);   //获取合适的JPEG格式的支持的图像尺寸
        for(Size size : imageSizes){
            float f = (float) size.getWidth() / size.getHeight();

            setLog(size.toString() + ",jpeg ratio:" + f);

            if(((f > ratio)?(f - ratio):(ratio - f)) < ((r > ratio)?(r - ratio):(ratio - r))){
                pictureSize = size;
                r = f;
            }
        }
        r = 0;
        imageSizes = map.getOutputSizes(ImageFormat.YV12);   //获取合适的YV12格式的支持的图像尺寸
        for(Size size : imageSizes){
            float f = (float) size.getWidth() / size.getHeight();

            setLog(size.toString() + ",yv12 ratio:" + f);

            if(((f > ratio)?(f - ratio):(ratio - f)) < ((r > ratio)?(r - ratio):(ratio - r))){
                previewSize = size;
                r = f;
            }
        }

        assert pictureSize != null;
        assert previewSize != null;
        setLog("picture size:" + pictureSize.toString()+ ",preview size:" + previewSize.toString());

        //设置ImageReader和监听
        //前三个参数分别是需要的尺寸和格式，最后一个参数代表每次最多获取几帧数据，本例的2代表ImageReader中最多可以获取两帧图像流

        //用于缓冲拍照图像数据
        pictureImageReader = ImageReader.newInstance(pictureSize.getWidth(),previewSize.getHeight(), ImageFormat.JPEG, 2);
        pictureImageReader.setOnImageAvailableListener(mOnImageAvailableListener,null);

        //用于缓冲预览图像数据
        previewImageReader = ImageReader.newInstance(previewSize.getWidth(),previewSize.getHeight(),ImageFormat.YV12,2);
        previewImageReader.setOnImageAvailableListener(previewOnImageAvailableListener,null);
    }

    //拍照
    private final Button.OnClickListener mButtonClickListener = new Button.OnClickListener(){

        @Override
        public void onClick(View view) {
            //拍照(初始化和发送拍照捕获请求)
            captureStillPicture();
            setLog("take picture");
        }
    };

    //静态图片捕获
    private void captureStillPicture() {
        try {
            //确保相机设备已打开，即满足拍照一定是在预览期间
            if (mCameraDevice == null) {
                return;
            }
            // 创建作为拍照的CaptureRequest.Builder
            CaptureRequest.Builder mPictureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            // 将ImageReader的surface作为CaptureRequest.Builder的目标，目的是实现ImageReader的回调与CaptureRequest进行关联
            mPictureRequestBuilder.addTarget(pictureImageSurface);

            //此处可以通过对捕获请求构建器进行相关设置，以实现各种图像数据捕获模式
            //mBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);// 设置自动对焦模式
            //mBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);// 设置自动曝光模式
            //mBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);//设置为自动模式
            //setFlashMode(CONTROL_AE_MODE);

            //因为：每个图像捕获对话（CameraCaptureSession）与相机设备（CameraDevice）之间的管道（pipeline）只能允许一个捕获请求（CaptureRequest）
            //但是：capture在底层的优先级高于setRepeatingRequest
            //所以：可以同时存在，不需要停止预览

            // 停止连续取景（停止预览）
            //mCaptureSession.stopRepeating();
            // 捕获静态图像（开始拍照，即发送一次捕获图像请求）
            mCaptureSession.capture(mPictureRequestBuilder.build(), new CameraCaptureSession.CaptureCallback() {
                // 拍照完成后回调
                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                    //无需停止预览
                }
            }, null);
        } catch (CameraAccessException e) {
            setLog("fail build capture");
            e.printStackTrace();
        }
        setLog("end capture");
    }

    //预览控件的回调
    private final TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture texture, int width, int height) {
            viewWidth = width;
            viewHeight = height;
            openCamera();   //打开相机
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture texture, int width, int height) {
            //configureTransform(width, height);  //更改尺寸
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture texture) {
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture texture) {
        }
    };

    //打开相机（对相机进行初始化）
    private void openCamera() {
        //检查相机权限
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            checkPermission();
            return;
        }
        //可用对参数进行相关了解
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            //打印可用的摄像头
            String[] CameraIdList = manager.getCameraIdList();
            for(String cameraId : CameraIdList){
                Log.d(TAG,"Camera Id :" + cameraId);
            }
            String mCameraId = CameraIdList[0];
            //打印所用摄像头的属性（相关属性）
            CameraCharacteristics cameraCharacteristics = manager.getCameraCharacteristics(mCameraId);
            map = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            int[] num = map.getOutputFormats(); //获取支持的图像格式
            for(int id : num){
                setLog("id : " + id);
            }
            Size[] imageSizes = map.getOutputSizes(ImageFormat.YV12);   //获取YV12格式的支持的图像尺寸
            for(Size sizes : imageSizes){
                setLog("YV12 sizes : "+ sizes.getWidth() + "*" + sizes.getHeight());
            }
            imageSizes = map.getOutputSizes(ImageFormat.JPEG);   //获取YV12格式的支持的图像尺寸
            for(Size sizes : imageSizes){
                setLog("JPEG sizes : "+ sizes.getWidth() + "*" + sizes.getHeight());
            }

            //图像载体操作
            initImageReaders();

            //获取可用相机设备列表
            manager.openCamera(mCameraId, mStateCallback, null);    //打开什么编号的相机，设备状态回调，是否需要另开线程
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    //关闭相机
    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            if (null != mCaptureSession) {
                mCaptureSession.close();
                mCaptureSession = null;
            }
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
            if (null != pictureImageReader) {
                pictureImageReader.close();
                pictureImageReader = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    //相机设备，用于捕获相机设备状态变化的回调对象（回调时，自动获取CameraDevice实例）
    private CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            // This method is called when the camera is opened.  We start camera preview here.
            mCameraOpenCloseLock.release();
            mCameraDevice = cameraDevice;
            startPreview();
        }

        @Override
        public void onDisconnected(CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(CameraDevice cameraDevice, int error) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }
    };

    //开始预览（对预览进行初始化）
    private void startPreview() {
        setLog("start preview");

        //通过TextureView实例（控件）获取一个SurfaceTexture实例，而该实例就是图像缓冲区
        SurfaceTexture texture = mTextureView.getSurfaceTexture();
        assert texture != null;
        //设置图像缓冲区的默认大小
        texture.setDefaultBufferSize(mTextureView.getWidth(), mTextureView.getHeight());

        //获取Texture的Surface
        previewTextureSurface = new Surface(texture);
        //获取ImageReader的Surface
        pictureImageSurface = pictureImageReader.getSurface();
        //获取预览ImageReader的Surface
        previewImageSurface = previewImageReader.getSurface();

        //通过向摄像机设备提供目标输出曲面集来创建新的摄像机捕获会话。
        try {
            mCameraDevice.createCaptureSession(Arrays.asList(previewTextureSurface,pictureImageSurface,previewImageSurface), mSessionStateCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    //接收有关摄像头捕获会话状态的更新
    private CameraCaptureSession.StateCallback mSessionStateCallback = new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(@NonNull CameraCaptureSession session) {
            //当摄像机设备完成自身配置时，将调用此方法，并且会话可以开始处理捕获请求。确定相机设备已打开
            if (null == mCameraDevice) {
                return;
            }
            mCaptureSession = session;
            try {
                //相机设备（CameraDevice）以预览（TEMPLATE_PREVIEW）的方式创建 捕获请求构建器（CaptureRequest.Builder）
                try {
                    mPreviewRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
                //将表面（surface）添加到此请求的目标列表中
                mPreviewRequestBuilder.addTarget(previewTextureSurface);
                mPreviewRequestBuilder.addTarget(previewImageSurface);

                //提交要由相机设备捕获的图像的请求。
                // Auto focus should be continuous for camera preview.
                //mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                // Finally, we start displaying the camera preview.
                CaptureRequest mPreviewRequest = mPreviewRequestBuilder.build();
                //session.capture(mPreviewBuilder.build(), mSessionCaptureCallback, mHandler);
                //请求通过此捕获会话无休止地重复捕获图像。（预览 连拍）   （开始预览）
                mCaptureSession.setRepeatingRequest(mPreviewRequest, mCaptureCallback,null);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession session) {}
    };

    //跟踪CaptureRequest提交到摄像机设备的进度的回调对象（预览捕获回调）
    private CameraCaptureSession.CaptureCallback mCaptureCallback = new CameraCaptureSession.CaptureCallback() {

                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                    //当图像捕获完全完成且所有结果元数据都可用时，将调用此方法。（预览数据处理result）
                }

                @Override
                public void onCaptureProgressed(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull CaptureResult partialResult){
                    //当图像捕获部分向前进展时，调用此方法; 可以获得图像捕获的一些（但不是全部）结果。
                }
    };


    @Override
    protected void onResume() {
        super.onResume();
        startBackgroundThread();

        //活动重现，开启相机
        if (mTextureView.isAvailable()) {
            openCamera();
        } else {
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    @Override
    public void onPause() {
        //活动停止，关闭相机
        closeCamera();
        stopBackgroundThread();
        super.onPause();
    }


    private HandlerThread mBackgroundThread;    //线程控制
    private Handler mBackgroundHandler;         //线程标识符
    //开启后台线程
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }
    //关闭后台线程
    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setLog(String arg){
        Log.d(TAG,arg);
    }

    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @SuppressLint("SdCardPath")
        @Override
        public void onImageAvailable(ImageReader reader) {

            //mFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg");
            String path = "/sdcard/api2_" + pictureSize.toString() + ".jpg";
            mFile = new File(path);
            //开启子线程执行保存操作
            mBackgroundHandler.post(new ImageSaver(reader.acquireNextImage(), mFile));
        }
    };


    private final ImageReader.OnImageAvailableListener previewOnImageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @SuppressLint("SdCardPath")
        @Override
        public void onImageAvailable(ImageReader imageReader) {

            String path = "/sdcard/api2_" + previewSize.toString() + ".yv12";
            mFile = new File(path);
            mBackgroundHandler.post(new PreviewSaver(imageReader.acquireLatestImage(),mFile));

        }
    };

    private static class ImageSaver implements Runnable {

        private final Image mImage;
        private final File mFile;

        ImageSaver(Image image, File file) {
            mImage = image;
            mFile = file;
        }
        @Override
        public void run() {
            ByteBuffer buffer = mImage.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
            FileOutputStream output = null;
            try {
                output = new FileOutputStream(mFile);
                output.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                mImage.close();
                if (null != output) {
                    try {
                        output.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private static class PreviewSaver implements Runnable{
        /**
         * 保存预览原始文件
         * 注意：一定要调用reader.acquireLatestImage()和close()方法，否则画面就会卡住
         */
        private final Image image;
        private final File file;
        PreviewSaver(Image mImage,File mFile){
            image = mImage;
            file = mFile;
        }
        @Override
        public void run() {
            //从image中取出bytes
            ///ByteBuffer buffer = image.getPlanes()[0].getBuffer();
            //byte[] bytes = new byte[buffer.remaining()];
            //buffer.get(bytes);

            //此处仅为Image到yv12的转变，具有的特殊性，即数据格式按yv12存储，像素间距默认为1

            final Image.Plane[] planes = image.getPlanes();

            int width = image.getWidth();
            int height = image.getHeight();

            int size = width * height;

            byte[] yuvBytes = new byte[size * 3 / 2];

            //目标数组的装到的位置
            int dstIndex = 0;

            //临时存储uv数据的
            byte uBytes[] = new byte[width * height / 4];
            byte vBytes[] = new byte[width * height / 4];
            int uIndex = 0;
            int vIndex = 0;

            int rowStride;
            for (int i = 0; i < planes.length; i++) {

                //pixelsStride = planes[i].getPixelStride();    //像素步幅 = 1 ,即像素与像素之间无间隔，而yv12都为1
                rowStride = planes[i].getRowStride();           //行间距，即每行在内存上的大小（考虑到行补齐）

                //Log.d(TAG,"像素步幅："+String.valueOf(pixelsStride)+";行间距："+String.valueOf(rowStride));

                ByteBuffer buffer = planes[i].getBuffer();

                //源数据的索引，y的数据是byte中连续的，u的数据是v向左移以为生成的，两者都是偶数位为有效数据
                byte[] bytes = new byte[buffer.capacity()];
                buffer.get(bytes);

                int srcIndex = 0;
                if (i == 0) {
                    //直接取出来所有Y的有效区域，也可以存储成一个临时的bytes，到下一步再copy
                    for (int j = 0; j < height; j++) {
                        System.arraycopy(bytes, srcIndex, yuvBytes, dstIndex, width);
                        srcIndex += rowStride;
                        dstIndex += width;
                    }
                } else if (i == 1) {
                    //逐行取相应的数据
                    for (int j = 0; j < height / 2; j++) {
                        System.arraycopy(bytes, srcIndex, uBytes, uIndex, width / 2);
                        srcIndex += rowStride;
                        uIndex += width / 2;
                    }

                } else if (i == 2) {
                    //逐行取相应的数据
                    for (int j = 0; j < height / 2; j++) {
                        System.arraycopy(bytes, srcIndex, vBytes, vIndex, width / 2);
                        srcIndex += rowStride;
                        vIndex += width / 2;
                    }
                }
            }
            System.arraycopy(vBytes, 0, yuvBytes, dstIndex, vBytes.length);
            System.arraycopy(uBytes, 0, yuvBytes, dstIndex + vBytes.length, uBytes.length);

            Log.d(TAG,"each preview");

            if(! file.exists()){
                //创建文件
                try{
                    FileOutputStream out = new FileOutputStream(file);  //若文件不存在，则创建并写入第一帧数据
                    out.write(yuvBytes);
                    out.close();
                    //Log.d(TAG,"create_file_succeed");
                }catch (IOException e){
                    e.printStackTrace();
                    Log.d(TAG,"create_file__fail");
                }
            } else{
                //追加数据
                try{
                    RandomAccessFile randomFile = new RandomAccessFile(file,"rw");  //若文件存在，则以追加的方式写入数据
                    long fileLength = randomFile.length();
                    randomFile.seek(fileLength);
                    randomFile.write(yuvBytes);
                    randomFile.close();
                    //Log.d(TAG,"store_normal");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            image.close();
        }
    }

}
